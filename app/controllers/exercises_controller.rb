class ExercisesController < ApplicationController
  
  def index
    if params[:search]
      @exercises = Exercise.search(params[:search]).order("created_at DESC")
    #never hit this else currently
    else
      @exercises = Exercise.order("created_at DESC")
    end
  end
  
  def create
    @exercise = Day.find(params[:exercise][:day_id]).exercises.build(exercise_params)
    if @exercise.save
      flash[:success] = "Exercise created!"
      redirect_to user_path(current_user)
    else
      render 'static_pages/home'
    end
  end
  
  def destroy
    @exercise = Exercise.find_by(id: params[:id])
    @exercise.destroy
    flash[:success] = "Exercise deleted"
    redirect_to request.referrer || user_path(current_user)
  end
  
  private

    def exercise_params
      params.require(:exercise).permit(:exercise_name, :weight, :reps, :sets, :comments)
    end

end
