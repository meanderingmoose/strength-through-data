class StaticPagesController < ApplicationController
  def home
    @day = current_user.days.build if logged_in?
  end

  def help
  end
  
  def about
  end
end
