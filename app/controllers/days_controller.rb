class DaysController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]
  before_action :correct_user,   only: :destroy
  
  def edit
    @day = Day.find(params[:id])
    @exercise = @day.exercises.build
  end
  
  def create
    @day = current_user.days.build(day_params)
    if @day.save
      flash[:success] = "Day created!"
      redirect_to user_path(current_user)
    else
      render 'static_pages/home'
    end
  end

  def destroy
    @day.destroy
    flash[:success] = "Day deleted"
    redirect_to request.referrer || root_url
  end
  
  private

    def day_params
      params.require(:day).permit(:content, :workout_date)
    end
    
    def correct_user
      @day = current_user.days.find_by(id: params[:id])
      redirect_to root_url if @day.nil?
    end
end
