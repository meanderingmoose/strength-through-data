class Day < ActiveRecord::Base
  belongs_to :user
  has_many :exercises, dependent: :destroy
  default_scope -> { order(workout_date: :desc) }
  validates :content, presence: true
  validates :user_id, presence: true
end
