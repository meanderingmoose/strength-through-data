class Exercise < ActiveRecord::Base
  belongs_to :day
  validates :exercise_name, presence: true
  before_save { |exercise| exercise.exercise_name = exercise_name.split.map(&:capitalize).join(' ') } 
  #from http://stackoverflow.com/questions/13520162/ruby-capitalize-every-word-first-letter
  
  def self.search(query)
    # where(:exercise_name, query) -> This would return an exact match of the query
    where("exercise_name like ?", "%#{query}%") 
  end
end
