class CreateDays < ActiveRecord::Migration
  def change
    create_table :days do |t|
      t.date :workout_date
      t.text :content
      t.references :user, index: true

      t.timestamps null: false
    end
    add_foreign_key :days, :users
    add_index :days, [:user_id, :created_at]
  end
end
