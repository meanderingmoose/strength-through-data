class CreateExercises < ActiveRecord::Migration
  def change
    create_table :exercises do |t|
      t.string :exercise_name
      t.integer :weight
      t.integer :sets
      t.integer :reps
      t.text :comments
      t.references :day, index: true

      t.timestamps null: false
    end
    add_foreign_key :exercises, :days
  end
end
